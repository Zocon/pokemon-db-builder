
const axios = require('axios');

import { BulbapediaParser } from "./bulbapediaParser";
const sqlWriter = require('./sqlWriter');
const { TypeData, Move, Ability, Pokemon, RegionalForm } = require('./models');

type TypeData = InstanceType<typeof TypeData>;
type Move = InstanceType<typeof Move>;
type Ability = InstanceType<typeof Ability>;
type Pokemon = InstanceType<typeof Pokemon>;
type RegionalForm = InstanceType<typeof RegionalForm>;

const baseUrl = 'https://bulbapedia.bulbagarden.net';

export class BulbapediaCrawler {

  public async fetchAll() {
    const startPage = '/wiki/Bulbasaur_(Pokémon)';
    var types : TypeData = await this.fetchTypes();
    var moves : Move[] = await this.fetchMoves();
    var abilities : Ability[] = await this.fetchAbilities();
    var regionalForms : RegionalForm[] = await this.fetchRegionalForms();
  };

  public async fetchTypes() : Promise<TypeData> {
    const startPage = '/wiki/Type';
    var url = baseUrl + startPage;
    var res = await this.fetchData(url);
    return BulbapediaParser.crawlTypes(res.data);
  };

  public async fetchMoves() : Promise<Move[]> {
    const startPage = '/wiki/List_of_moves';
    var url = baseUrl + startPage;
    var res = await this.fetchData(url);
    return BulbapediaParser.crawlMoves(res.data);
  };

  public async fetchAbilities() : Promise<Ability[]> {
    const startPage = '/wiki/Ability';
    var url = baseUrl + startPage;
    var res = await this.fetchData(url);
    return BulbapediaParser.crawlAbilities(res.data);
  };

  public async fetchRegionalForms() : Promise<RegionalForm[]> {
    const startPage = '/wiki/Regional_form';
    var url = baseUrl + startPage;
    var res = await this.fetchData(url);
    return BulbapediaParser.crawlRegionalForms(res.data);
  };

  private async sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  };
  
  private async fetchData(url) {
    console.log("Fetching data...");
    let response = await axios(url).catch((err) => console.log(err));
  
    if (response.status !== 200) {
      console.log("Error occurred while fetching data");
      console.log(response.status + ": " + response.statusText);
      return;
    }
  
    return response;
  };
};