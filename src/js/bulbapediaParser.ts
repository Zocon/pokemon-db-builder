import * as cheerio from 'cheerio';

import { TypeData, Move, Ability, Pokemon, PokemonForm, FormName } from "./models";

const FORMS = ['Alolan','Galarian','Hisuian'];

export class BulbapediaParser {

  public static crawlTypes(html: string) : TypeData {
    const $ = cheerio.load(html);
    var typeHeader = $('#Type_chart');
    var typeTable = typeHeader.parent().next().next().next().next()[0].children[1];

    var types = [];
    var typeChart = [];
    var defTypeRow = typeTable.children[2];
    for (var d=0; d<defTypeRow.children.length; d++) {
      if (defTypeRow.children[d].type !== 'text') {
        types.push(defTypeRow.children[d].children[0].attribs.title);
      }
    }

    for (var a=3; a<typeTable.children.length-1; a++) {
      var attackRow = typeTable.children[a];
      if (attackRow.type === 'text') {
        continue;
      }
      var d = -1;
      var attackingType = '';
      for (var e=0; e<attackRow.children.length; e++) {
        var element = attackRow.children[e];
        if (element.type === 'text') {
          continue;
        }
        if (element.attribs.rowspan > 0) {
          // row header 'Attacking type'
          continue;
        }

        if (d < 0) {
          attackingType = element.children[0].attribs.title;

        } else if (d < types.length) {
          var defendingType = types[d];
          var multiplier = element.children[0].data;
          multiplier = multiplier.substring(0,multiplier.length-2);
          if (multiplier == '½') {
            multiplier = '.25';
          }
          multiplier = Number(multiplier);

          typeChart.push({
            attackingType: attackingType,
            defendingType: defendingType,
            multiplier: multiplier
          });

        } else {
          console.log('Warning: too many columns in this row!');
        }

        d++;
      }
    }

    return {
      types: types,
      typeChart: typeChart
    };
  };

  public static crawlMoves(html: string) : Move[] {
    const $ = cheerio.load(html);
    var movesHeader = $('#List_of_moves');
    var moveTable = movesHeader.parent().next().next()[0].children[1].children[0].children[1].children[1].children[1];

    var moves: Move[] = [];
    for (var m=2; m<moveTable.children.length; m++) {
      var moveRow = moveTable.children[m];
      if (moveRow.type === 'text') {
        continue;
      }

      var id = parseInt(moveRow.children[1].children[0].data);
      if (isNaN(id)) {
        continue;
      }

      var power = parseInt(moveRow.children[13].children[0].data);
      if (isNaN(power)) {
        power = null;
      }

      var accuracyNode = moveRow.children[15].children[0];
      if (!accuracyNode.data) {
        accuracyNode = accuracyNode.children[0];
      }
      var accuracy = parseInt(accuracyNode.data.replace("%","").replace("\n",""))
      if (isNaN(accuracy)) {
        accuracy = null;
      }

      var move = {
        id: id,
        name: moveRow.children[3].children[0].children[0].data.replace(/'/g, "''"),
        type: moveRow.children[5].children[0].children[0].children[0].data,
        category: moveRow.children[7].children[0].children[0].children[0].data,
        pp: parseInt(moveRow.children[11].children[0].data),
        power: power,
        accuracy: accuracy,
        gen: moveRow.children[17].children[0].data.replace("\n","")
      };

      moves.push(move);
    }

    return moves;
  };

  public static crawlAbilities(html: string) : Ability[] {
    const $ = cheerio.load(html);
    var abilitiesHeader = $('#List_of_Abilities');
    var abilitiesTable = abilitiesHeader.parent().next()[0].children[1].children[0].children[1].children[1].children[1];

    var abilities: Ability[] = [];
    for (var a=2; a<abilitiesTable.children.length; a++) {
      var abilityRow = abilitiesTable.children[a];
      if (abilityRow.type === 'text') {
        continue;
      }

      //console.log(abilityRow.children[1]);
      var idNode = abilityRow.children[1].children[0];
      if (!idNode.data) {
        idNode = idNode.children[0];
      }

      // ability 266/267 are identical except for id
      for (var id of idNode.data.split('/')) {
        id = parseInt(id);
        if (isNaN(id)) {
          continue;
        }
    
        var ability = {
          id: id,
          name: abilityRow.children[3].children[0].children[0].data.replace(/'/g, "''"),
          desc: abilityRow.children[5].children[0].data.replace(/'/g, "''").replace("\n", ""),
          gen: abilityRow.children[7].children[0].data.replace("\n", "")
        }
    
        abilities.push(ability);
      }
    }

    return abilities;
  };

  public static crawlRegionalForms(html: string) : PokemonForm[] {
    // const startPage = '/wiki/Regional_form';
    // url = baseUrl + startPage;
    // var res = await fetchData(url);
    // const $ = cheerio.load(res.data);
    const $ = cheerio.load(html);
    var alolanHeader = $('#List_of_Alolan_Forms');
    var alolanTable = alolanHeader.parent().next().next()[0].children[1];

    // skip the header rows
    for (var r=4; r<alolanTable.children.length; r++) {
      var row = alolanTable.children[r];
      if (row.type === 'text') {
        continue;
      }

      var pokemon = row[1];
      var normalType = row[3];
      var normalAbilities = row[5];
      var alolanType = row[9];
      var alolanAbilities = row[11];

      console.log(row);
      break;
    }

    var galarianHeader = $('#List_of_Galarian_Forms');
    var hisuianHeader = $('#List_of_Hisuian_Forms');

    return null;
  };

  public static crawlPokemon(html: string, wiki: string) : Pokemon {
    var pokemon : Pokemon;
  
    var nextPage = '';
    const root = cheerio.load(html);
    const $ = cheerio.load(root('#mw-content-text .mw-parser-output').html());
 
    // get the next page 
    var dexNav = cheerio.load($('table')[0]);
    nextPage = dexNav('table table')[2].children[1].children[0].children[1].children[0].attribs.href;
 
    // get the 'at a glance' data
    var atAGlanceNode = $('table > tbody')[0];
  
    this.scrapeBasicInfo(atAGlanceNode.children[0], pokemon);
    this.scrapeTypeInfo(atAGlanceNode.children[2], pokemon);
    this.scrapeAbilityInfo(atAGlanceNode.children[4], pokemon);
  
    var baseStats = $('#Base_stats')[0];
    if (!baseStats) {
      baseStats = $('#Base_Stats')[0];
    }
  
    this.scrapeBaseStats(baseStats, pokemon);
  
    var learnset = $('#Learnset')[0];
  
    this.scrapeLearnset(learnset, pokemon);
  
    return pokemon;
  };

  private static scrapeBasicInfo(node, pokemon: Pokemon) {
    var commonNode = node.children[1].children[1].children[1].children[0];
    var rowNode = commonNode.children[1].children[1].children[1].children[0].children[1];
    pokemon.name = rowNode.children[0].children[0].children[0].children[0].data;
    pokemon.species = '';
    var species = rowNode.children[2].children[0];
    var numVariants = (species.children.length > 1) ? 2 : 1;
    if (numVariants === 1) {
      pokemon.species = species.children[0].data;
    } else {
      pokemon.species = species.children[0].children[0].data;
      var formName : FormName = this.parseFormName(species.children[3].attribs.title, pokemon.name);
      var form : PokemonForm = this.findForm(pokemon, formName);
      if (form === null) {
        form = new PokemonForm();
        form.pokemon = formName.pokemon;
        form.region = formName.region;
        form.form = formName.form;
      }
      form.species = species.children[3].children[0].data
    }
    pokemon.dex = parseInt(commonNode.children[3].children[0].children[0].children[0].children[0].children[0].data.substring(1));
  };

  private static scrapeTypeInfo(node, pokemon: Pokemon) {
    var table = node.children[1].children[2].children[1];
    for (var r=0; r<table.children.length; r++) {
      var row = table.children[r];
      if (row.type !== 'tag') {
        continue;
      }
      for (var c=0; c<row.children.length; c++) {
        var col = row.children[c];
        if (col.type !== 'tag') {
          continue;
        }
        if (col.attribs.style && col.attribs.style.includes('display: none;')) {
          continue;
        }
  
        var typeTable = col.children[1].children[1].children[0];
        var types = [ typeTable.children[1].children[0].children[0].children[0].children[0].data ];
        var type2 = typeTable.children[3].children[0].children[0].children[0].children[0].data;
        if (type2 && type2 !== 'Unknown') {
          types.push(type2);
        }

        var formName : FormName = null;
        if (col.children.length === 4) {
          // multiple forms, also grab 
          var formTitle = col.children[2];
          if (formTitle.children.length > 0) {
            formName = this.parseFormName(formTitle.children[0].data, pokemon.name);
          }
        }

        if (formName && (formName.region || formName.form)) {
          var form = this.findForm(pokemon, formName);
          if (form === null) {
            form = new PokemonForm();
            form.name = pokemon.name;
            form.region = formName.region;
            form.form = formName.form;
            pokemon.forms.push(form);
          }
    
          form.types = types;
        } else {
          pokemon.types = types;
        }
      }
    }
  };

  private static scrapeAbilityInfo(node, data) {
    var rawAbilities = [];
    var table = node.children[1].children[2].children[1];
    for (var r=0; r<table.children.length; r++) {
      var row = table.children[r];
      if (row.type !== 'tag') {
        continue;
      }
      for (var c=0; c<row.children.length; c++) {
        var col = row.children[c];
        if (col.type !== 'tag') {
          continue;
        }
        if (col.attribs.style && col.attribs.style.includes('display: none')) {
          continue;
        }
  
        var rawForm = data.name;
        var abilitiesList = [];
  
        for (var a=0; a<col.children.length; a++) {
          var ability = col.children[a];
          if (ability.name === 'a') {
            abilitiesList.push(ability.children[0].children[0].data);
          } else if (ability.name === 'small') {
            rawForm = ability.children[0].data;
          }
        }
  
        rawAbilities.push({
          rawForm: rawForm,
          abilitiesList: abilitiesList
        });
      }
    }
  
    var hidden = form.includes('Hidden Ability');
    form.replace('Hidden Ability', '');
  
    var forms = rawForm.split(' and ');
    for (var form of forms) {
      var region = '';
      for (var f of FORMS) {
        if (form.includes(f)) {
          region = f;
        }
      }
      form = form.replace(region, '').replace(data.name, '');
  
      for (var ability of abilitiesList) {
        data.abilities.push({
          region: region,
          form: form,
          hidden: hidden,
          ability: ability
        });
      }
    }
  };

  private static scrapeBaseStats(node, data) {
    data.stats = {};
    var current = node.parent.next;
    var form = data.name;
    while (current.name !== 'h4' && current.name !== 'h3') {
      if (current.type === 'text') {
        current = current.next;
        continue;
  
      } else if (current.type === 'tag') {
        if (current.name === 'p') {
          current = current.next;
          continue;
  
        } else if (current.name === 'h5') {
          if (current.children[0].children.length > 0) {
            form = current.children[0].children[0].data;
          } else {
            form = current.children[1].children[0].data;
          }
  
        } else if (current.name === 'table') {
          var table = current.children[1];
          var stats = {
            hp: parseInt(table.children[4].children[1].children[1].children[0].data),
            attack: parseInt(table.children[6].children[1].children[1].children[0].data),
            defense: parseInt(table.children[8].children[1].children[1].children[0].data),
            spAttack: parseInt(table.children[10].children[1].children[1].children[0].data),
            spDefense: parseInt(table.children[12].children[1].children[1].children[0].data),
            speed: parseInt(table.children[14].children[1].children[1].children[0].data)
          }
  
          data.stats[form] = stats;
          form = data.name;
  
        } else {
          console.log('\nMissed name');
          console.log(current);
          break;
        }
  
      } else {
        console.log('\nMissed type');
        console.log(current);
        break;
      }
  
      current = current.next;
    }
  };

  private static scrapeLearnset(node, data) {
    var current = node.parent.next;
    var sourceType = '';
    var form = data.name;
    data.moves = {};
    data.moves[form] = [];
    while (current.name !== 'h3') {
      if (current.type !== 'tag') {
        current = current.next;
        continue;
  
      } else {
        switch (current.name) {
        case 'h4':
          switch (current.children[0].attribs.id) {
          case 'By_leveling_up':
              sourceType = 'level';
            break;
          case 'By_TM/TR':
              sourceType = 'tm/tr';
            break;
          case 'By_breeding':
              sourceType = 'breeding';
            break;
          case 'By_tutoring':
              sourceType = 'tutoring';
            break;
          case 'By_transfer_from_another_generation':
          case 'TCG-only_moves':
          case 'Anime-only_moves':
          default:
              sourceType = 'ignore';
          }
          // Since we moved to a new h4, we aren't looking at the form for the previous h5
          // Reset to the default (pokemon name), until we see another h5
          form = data.name;
          break;
  
        case 'h5':
          // Some h5 elements were given 2 children, where the second had the actual form text.
          // Check if the first child has text data, and use that if it exists, otherwise get it from the second child
          if (current.children[0].children.length > 0) {
            form = current.children[0].children[0].data;
          } else {
            form = current.children[1].children[0].data;
          }
  
          // initialize moves to an empty array so we can just append to the array later
          if (!data.moves[form]) {
            data.moves[form] = [];
          }
          break;
  
        case 'table':
          // these variables capture the relevant structure differences between the tables
          var moveNodeNumber = 3;
          var parseSource = (row) => null;
          switch(sourceType) {
          case 'level':
            parseSource = (row) => parseInt(row.children[1].children[0].children[0].data);
            break;
  
          case 'tm/tr':
            moveNodeNumber = 5;
            parseSource = (row) => row.children[3].children[0].attribs.title;
            break;
  
          case 'breeding':
            parseSource = function(row) {
              var source = [];
              var parents = row.children[1];
              for (var p=0; p<parents.children.length; p++) {
                var parent = parents.children[p];
                if (parent.name == 'a') {
                  source.push(parent.attribs.title.replace(/'/g,"''"));
                }
              }
              return source;
            };
            break;
  
          case 'tutoring':
            moveNodeNumber = 7;
            break;
  
          default:
            console.log(`Skipping table for source type ${sourceType} because this is not a recognized type`);
            current = current.next;
            continue;
          }
  
          var table = current.children[1].children[2].children[1].children[1].children[1];
          // skip the header row
          for (var r=2; r<table.children.length; r++) {
            var row = table.children[r];
            if (row.type === 'text' || !row.children[moveNodeNumber]) {
              continue;
            }
  
            var moveNode = row.children[moveNodeNumber].children[0].children[0].children[0];
            if (!moveNode.data) {
              // STAB moves are wrapped in an extra <b>
              moveNode = moveNode.children[0];
            }
  
            var move = {
              move: moveNode.data.replace(/'/g,"''"),
              sourceType: sourceType,
              source: parseSource(row)
            };
            data.moves[form].push(move);
          }
          break;
  
        default:
          console.log(`\nUnrecognized tag: ${current.name}`);
        }
      }
  
      current = current.next;
    }
  };

  private static findForm(pokemon: Pokemon, formName: FormName) : PokemonForm {
    if (pokemon.forms) {
      for (var form of pokemon.forms) {
        if (form.region === formName.region && form.form === formName.form) {
          return form;
        }
      }
    }

    return null;
  };

  private static parseFormName(fullName: string, pokemonName: string) : FormName {
    var formName = new FormName();
    formName.pokemon = pokemonName;

    formName.region = '';
    for (var f of FORMS) {
      if (fullName.includes(f)) {
        formName.region = f;
      }
    }

    formName.form = fullName.replace(pokemonName, '').replace(formName.region, '').trim();

    return formName;
  }
};