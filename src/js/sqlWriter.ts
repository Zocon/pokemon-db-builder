import * as fs from 'fs';

import { TypeData, Move, Ability } from "./models";

export class SqlWriter {
  async writeTypes(typeData: TypeData) {
    const outputFile = 'build/types.sql';

    fs.writeFileSync(outputFile, 
      `DROP TABLE IF EXISTS TYPES;

CREATE TABLE TYPES (
  ID INTEGER PRIMARY KEY,
  NAME TEXT NOT NULL
);

DROP TABLE IF EXISTS TYPE_CHART;

CREATE TABLE TYPE_CHART (
  ATTACKING_TYPE TEXT NOT NULL,
  DEFENDING_TYPE TEXT NOT NULL,
  MULTIPLIER NUMBER NOT NULL,
  UNIQUE KEY TYPE_MATCHUP (ATTACKING_TYPE, DEFENDING_TYPE)
);

`);

    for (var t=0; t<typeData.types.length; t++) {
      fs.appendFileSync(outputFile, `INSERT INTO TYPES (ID,NAME) VALUES (${t},'${typeData.types[t]}');\n`);
    }
    fs.appendFileSync(outputFile, '\n');
    for (var c=0; c<typeData.typeChart.length; c++) {
      fs.appendFileSync(outputFile, `INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('${typeData.typeChart[c].attackingType}','${typeData.typeChart[c].defendingType}',${typeData.typeChart[c].multiplier});\n`);
    }
  }

  async writeMoves(moves: Move[]) {

    // now that we've got all the data, write the file
    const outputFile = 'build/moves.sql';

    fs.writeFileSync(outputFile, 
      `DROP TABLE IF EXISTS MOVES;
      
CREATE TABLE MOVES (
  ID INTEGER PRIMARY KEY,
  NAME TEXT NOT NULL UNIQUE,
  TYPE TEXT NOT NULL,
  CATEGORY TEXT NOT NULL,
  PP INTEGER NOT NULL,
  POWER INTEGER,
  ACCURACY INTEGER,
  GENERATION TEXT NOT NULL
);

`);

    for (var m=0; m<moves.length; m++) {
      var move = moves[m];
      fs.appendFileSync(outputFile, `INSERT INTO MOVES (ID,NAME,TYPE,CATEGORY,PP,POWER,ACCURACY,GENERATION) VALUES (${move.id},'${move.name}','${move.type}','${move.category}',${move.pp},${move.power},${move.accuracy},'${move.gen}');\n`);
    }
  }

  async writeAbilities(abilities: Ability[]) {
    // now that we've got all the data, write the file
    const outputFile = 'build/abilities.sql';

    fs.writeFileSync(outputFile, 
      `DROP TABLE IF EXISTS ABILITIES;
      
CREATE TABLE ABILITIES (
  ID INTEGER PRIMARY KEY,
  NAME TEXT NOT NULL UNIQUE,
  DESCRIPTION TEXT NOT NULL,
  GENERATION TEXT NOT NULL
);

`);

    for (var a=0; a<abilities.length; a++) {
      var ability = abilities[a];
      fs.appendFileSync(outputFile, `INSERT INTO ABILITIES (ID,NAME,DESCRIPTION,GENERATION) VALUES (${ability.id},'${ability.name}','${ability.desc}','${ability.gen}');\n`);
    }
  }
}