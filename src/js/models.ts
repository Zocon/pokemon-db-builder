export class TypeMatchup {
  attackingType: string;
  defendingType: string;
  multiplier: number
};

export class TypeData {
  types: string[];
  typeChart: TypeMatchup[];
};

export class Move {
  id: number;
  name: string;
  type: string;
  category: string;
  pp: number;
  power: number | null;
  accuracy: number | null;
  gen: string;
};

export class Ability {
  id: number;
  name: string;
  desc: string;
  gen: string;
}

export class Stats {
  hp: number;
  attack: number;
  defense: number;
  specialAttack: number;
  specialDefense: number;
  speed: number;
}

export abstract class LearnedMove {
  name: string;
  learnedFrom: string;
  learnedAt: any;
}

export class LevelLearnedMove extends LearnedMove {
  learnedFrom = 'level';
  learnedAt: number;
}

export class TMLearnedMove extends LearnedMove {
  learnedFrom = 'TM/TR';
  learnedAt: string;
}

export class BreedingLearnedMove extends LearnedMove {
  learnedFrom = 'breeding';
  learnedAt: string[];
}

export class TutoringLearnedMove extends LearnedMove {
  learnedFrom = 'tutor';
  learnedAt = null;
}

export class Pokemon {
  dex: number;
  name: string;

  species: string;
  types: string[] | null;
  abilities: string[] | null;
  hiddenAbility: string | null;
  moves: LearnedMove[] | null;
  stats: Stats | null;

  forms: PokemonForm[] = [];

  wiki: string;
  next: string;
}

export class PokemonForm {
  dex: number;
  pokemon: string;
  region: string | null;
  form: string | null;
  species: string | null;
  types: string[] | null;
  abilities: string[] | null;
  hiddenAbility: string | null;
  moves: LearnedMove[] | null;
  stats: Stats | null;
}

export class FormName {
  pokemon: string;
  region: string | null;
  form: string | null;
}