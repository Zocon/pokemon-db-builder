DROP TABLE IF EXISTS TYPES;
    
CREATE TABLE TYPES (
  ID INTEGER PRIMARY KEY,
  NAME TEXT NOT NULL
);

DROP TABLE IF EXISTS TYPE_CHART;

CREATE TABLE TYPE_CHART (
  ATTACKING_TYPE TEXT NOT NULL,
  DEFENDING_TYPE TEXT NOT NULL,
  MULTIPLIER NUMBER NOT NULL,
  UNIQUE KEY TYPE_MATCHUP (ATTACKING_TYPE, DEFENDING_TYPE)
);

INSERT INTO TYPES (ID,NAME) VALUES (0,'Normal');
INSERT INTO TYPES (ID,NAME) VALUES (1,'Fighting');
INSERT INTO TYPES (ID,NAME) VALUES (2,'Flying');
INSERT INTO TYPES (ID,NAME) VALUES (3,'Poison');
INSERT INTO TYPES (ID,NAME) VALUES (4,'Ground');
INSERT INTO TYPES (ID,NAME) VALUES (5,'Rock');
INSERT INTO TYPES (ID,NAME) VALUES (6,'Bug');
INSERT INTO TYPES (ID,NAME) VALUES (7,'Ghost');
INSERT INTO TYPES (ID,NAME) VALUES (8,'Steel');
INSERT INTO TYPES (ID,NAME) VALUES (9,'Fire');
INSERT INTO TYPES (ID,NAME) VALUES (10,'Water');
INSERT INTO TYPES (ID,NAME) VALUES (11,'Grass');
INSERT INTO TYPES (ID,NAME) VALUES (12,'Electric');
INSERT INTO TYPES (ID,NAME) VALUES (13,'Psychic');
INSERT INTO TYPES (ID,NAME) VALUES (14,'Ice');
INSERT INTO TYPES (ID,NAME) VALUES (15,'Dragon');
INSERT INTO TYPES (ID,NAME) VALUES (16,'Dark');
INSERT INTO TYPES (ID,NAME) VALUES (17,'Fairy');

INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Normal','Normal',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Normal','Fighting',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Normal','Flying',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Normal','Poison',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Normal','Ground',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Normal','Rock',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Normal','Bug',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Normal','Ghost',0);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Normal','Steel',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Normal','Fire',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Normal','Water',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Normal','Grass',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Normal','Electric',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Normal','Psychic',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Normal','Ice',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Normal','Dragon',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Normal','Dark',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Normal','Fairy',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fighting','Normal',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fighting','Fighting',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fighting','Flying',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fighting','Poison',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fighting','Ground',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fighting','Rock',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fighting','Bug',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fighting','Ghost',0);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fighting','Steel',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fighting','Fire',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fighting','Water',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fighting','Grass',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fighting','Electric',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fighting','Psychic',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fighting','Ice',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fighting','Dragon',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fighting','Dark',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fighting','Fairy',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Flying','Normal',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Flying','Fighting',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Flying','Flying',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Flying','Poison',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Flying','Ground',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Flying','Rock',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Flying','Bug',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Flying','Ghost',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Flying','Steel',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Flying','Fire',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Flying','Water',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Flying','Grass',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Flying','Electric',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Flying','Psychic',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Flying','Ice',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Flying','Dragon',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Flying','Dark',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Flying','Fairy',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Poison','Normal',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Poison','Fighting',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Poison','Flying',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Poison','Poison',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Poison','Ground',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Poison','Rock',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Poison','Bug',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Poison','Ghost',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Poison','Steel',0);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Poison','Fire',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Poison','Water',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Poison','Grass',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Poison','Electric',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Poison','Psychic',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Poison','Ice',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Poison','Dragon',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Poison','Dark',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Poison','Fairy',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ground','Normal',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ground','Fighting',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ground','Flying',0);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ground','Poison',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ground','Ground',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ground','Rock',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ground','Bug',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ground','Ghost',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ground','Steel',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ground','Fire',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ground','Water',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ground','Grass',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ground','Electric',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ground','Psychic',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ground','Ice',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ground','Dragon',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ground','Dark',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ground','Fairy',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Rock','Normal',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Rock','Fighting',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Rock','Flying',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Rock','Poison',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Rock','Ground',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Rock','Rock',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Rock','Bug',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Rock','Ghost',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Rock','Steel',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Rock','Fire',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Rock','Water',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Rock','Grass',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Rock','Electric',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Rock','Psychic',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Rock','Ice',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Rock','Dragon',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Rock','Dark',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Rock','Fairy',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Bug','Normal',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Bug','Fighting',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Bug','Flying',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Bug','Poison',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Bug','Ground',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Bug','Rock',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Bug','Bug',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Bug','Ghost',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Bug','Steel',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Bug','Fire',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Bug','Water',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Bug','Grass',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Bug','Electric',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Bug','Psychic',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Bug','Ice',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Bug','Dragon',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Bug','Dark',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Bug','Fairy',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ghost','Normal',0);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ghost','Fighting',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ghost','Flying',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ghost','Poison',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ghost','Ground',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ghost','Rock',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ghost','Bug',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ghost','Ghost',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ghost','Steel',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ghost','Fire',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ghost','Water',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ghost','Grass',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ghost','Electric',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ghost','Psychic',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ghost','Ice',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ghost','Dragon',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ghost','Dark',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ghost','Fairy',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Steel','Normal',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Steel','Fighting',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Steel','Flying',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Steel','Poison',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Steel','Ground',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Steel','Rock',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Steel','Bug',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Steel','Ghost',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Steel','Steel',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Steel','Fire',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Steel','Water',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Steel','Grass',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Steel','Electric',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Steel','Psychic',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Steel','Ice',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Steel','Dragon',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Steel','Dark',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Steel','Fairy',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fire','Normal',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fire','Fighting',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fire','Flying',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fire','Poison',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fire','Ground',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fire','Rock',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fire','Bug',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fire','Ghost',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fire','Steel',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fire','Fire',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fire','Water',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fire','Grass',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fire','Electric',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fire','Psychic',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fire','Ice',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fire','Dragon',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fire','Dark',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fire','Fairy',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Water','Normal',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Water','Fighting',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Water','Flying',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Water','Poison',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Water','Ground',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Water','Rock',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Water','Bug',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Water','Ghost',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Water','Steel',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Water','Fire',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Water','Water',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Water','Grass',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Water','Electric',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Water','Psychic',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Water','Ice',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Water','Dragon',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Water','Dark',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Water','Fairy',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Grass','Normal',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Grass','Fighting',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Grass','Flying',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Grass','Poison',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Grass','Ground',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Grass','Rock',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Grass','Bug',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Grass','Ghost',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Grass','Steel',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Grass','Fire',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Grass','Water',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Grass','Grass',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Grass','Electric',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Grass','Psychic',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Grass','Ice',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Grass','Dragon',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Grass','Dark',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Grass','Fairy',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Electric','Normal',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Electric','Fighting',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Electric','Flying',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Electric','Poison',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Electric','Ground',0);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Electric','Rock',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Electric','Bug',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Electric','Ghost',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Electric','Steel',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Electric','Fire',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Electric','Water',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Electric','Grass',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Electric','Electric',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Electric','Psychic',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Electric','Ice',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Electric','Dragon',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Electric','Dark',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Electric','Fairy',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Psychic','Normal',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Psychic','Fighting',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Psychic','Flying',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Psychic','Poison',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Psychic','Ground',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Psychic','Rock',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Psychic','Bug',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Psychic','Ghost',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Psychic','Steel',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Psychic','Fire',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Psychic','Water',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Psychic','Grass',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Psychic','Electric',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Psychic','Psychic',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Psychic','Ice',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Psychic','Dragon',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Psychic','Dark',0);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Psychic','Fairy',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ice','Normal',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ice','Fighting',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ice','Flying',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ice','Poison',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ice','Ground',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ice','Rock',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ice','Bug',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ice','Ghost',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ice','Steel',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ice','Fire',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ice','Water',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ice','Grass',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ice','Electric',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ice','Psychic',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ice','Ice',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ice','Dragon',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ice','Dark',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Ice','Fairy',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dragon','Normal',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dragon','Fighting',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dragon','Flying',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dragon','Poison',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dragon','Ground',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dragon','Rock',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dragon','Bug',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dragon','Ghost',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dragon','Steel',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dragon','Fire',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dragon','Water',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dragon','Grass',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dragon','Electric',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dragon','Psychic',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dragon','Ice',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dragon','Dragon',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dragon','Dark',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dragon','Fairy',0);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dark','Normal',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dark','Fighting',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dark','Flying',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dark','Poison',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dark','Ground',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dark','Rock',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dark','Bug',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dark','Ghost',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dark','Steel',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dark','Fire',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dark','Water',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dark','Grass',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dark','Electric',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dark','Psychic',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dark','Ice',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dark','Dragon',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dark','Dark',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Dark','Fairy',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fairy','Normal',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fairy','Fighting',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fairy','Flying',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fairy','Poison',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fairy','Ground',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fairy','Rock',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fairy','Bug',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fairy','Ghost',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fairy','Steel',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fairy','Fire',0.25);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fairy','Water',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fairy','Grass',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fairy','Electric',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fairy','Psychic',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fairy','Ice',1);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fairy','Dragon',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fairy','Dark',2);
INSERT INTO TYPE_CHART (ATTACKING_TYPE,DEFENDING_TYPE,MULTIPLIER) VALUES ('Fairy','Fairy',1);
